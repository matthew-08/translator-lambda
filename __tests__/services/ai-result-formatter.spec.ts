import { AIResultFormatter } from '../../src/services/ai-result-formatter';
import { FormatConfig } from '../../src/services/config/format-config';

const mockInput = [
  [
    'lãnh đạo',
    'cấp cao',
    'ở',
    'một',
    'công ty',
    'lớn',
    'có lẽ',
    'bạn',
    'sẽ',
    'không',
    'cần',
    'đến',
    'lời',
    'khuyên',
  ],
  [
    'liên',
    'tục',
    'kiểm',
    'tra',
    'e-mail',
    'không',
    'nói',
    'lên',
    'được',
    'rằng',
    'bạn',
    'sẽ',
    'thành',
    'công',
    'nếu',
    'bắt',
  ],
];
const mockAiTranslationResult =
  "senior leadership at a large company maybe you won't need advice\ncontinuously checking email doesn't say that you'll succeed if you try";

describe('ai result formatter', () => {
  it('formats csv style into expected result', () => {
    const formatConfig = new FormatConfig({
      inputFormat: 'array',
      outputFormat: 'csv-style',
    });
    const resultFormatter = new AIResultFormatter({
      inputFormat: 'array',
      outputFormat: 'csv-style',
    });
    const res = resultFormatter.format({
      aiOutput: mockAiTranslationResult,
      originalInput: mockInput,
    })!;

    expect(res?.length).toEqual(2);
    expect(res.every(({ sentence }, index) => mockInput[index] === sentence));
    const mockAiTranslationResultArray = mockAiTranslationResult.split('\n');
    expect(
      res.every(
        ({ translation }, index) =>
          mockAiTranslationResultArray[index] === translation
      )
    );
  });
});
